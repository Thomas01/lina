import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-top-games',
  templateUrl: './top-games.component.html',
  styleUrls: ['./top-games.component.css']
})
export class TopGamesComponent implements OnInit {
  games : any[] = [];
  constructor(private api : ApiService) { }

  ngOnInit(): void {
    this.api.fetchGames().subscribe({
      next : (result) =>{
        result.forEach( (game: any) => {
         if(game.categories.includes("top")){

              // Get top category
             let top = game.categories.filter((category: any) => category ==='top')
              // Create variable play
             let play = !game.image.Play ? "Play" : ''
              // push variables
             this.games.push({category:top, image:game.image, play:play ,name:game.name, id:game.id})
             
          }
        });
      }
    })
  }

}
