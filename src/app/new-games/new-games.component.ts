import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-new-games',
  templateUrl: './new-games.component.html',
  styleUrls: ['./new-games.component.css']
})
export class NewGamesComponent implements OnInit {
  games: any[] =[];

  constructor( private api: ApiService) { }

  ngOnInit(): void {
    this.api.fetchGames().subscribe({
      next : (result) =>{
        result.forEach( (game: any) => {
         if(game.categories.includes("top")){

              // Get top category
             let nGames = game.categories.filter((category: any) => category ==='new')
              // Create variable play
             let play = !game.image.Play ? "Play" : ''
              // push variables
             this.games.push({category:nGames, image:game.image, play:play ,name:game.name, id:game.id})
              
          }
        });
      }
    })
  }

}
