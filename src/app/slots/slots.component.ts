import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-slots',
  templateUrl: './slots.component.html',
  styleUrls: ['./slots.component.css']
})
export class SlotsComponent implements OnInit {
  games : any[] = [];

  constructor(private api : ApiService) { }

  ngOnInit(): void {
    this.api.fetchGames().subscribe({
      next : (result) =>{
        result.forEach( (game: any) => {
         if(game.categories.includes("top")){

              // Get top category
             let slots = game.categories.filter((category: any) => category ==='slots')
              // Create variable play
             let play = !game.image.Play ? "Play" : ''
              // push variables
             this.games.push({category:slots, image:game.image, play:play ,name:game.name, id:game.id})
             
          }
        });
      }
    })
  }

}
