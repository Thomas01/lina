import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { jackportsInfo } from '../models/globalData';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-jackpots',
  templateUrl: './jackpots.component.html',
  styleUrls: ['./jackpots.component.css']
})
export class JackpotsComponent implements OnInit {

  
  jackPortsInfo!: Observable<jackportsInfo[]>;

  constructor(private api : ApiService) {}

  ngOnInit(): void {
    this.jackPortsInfo = this.api.getAllGame();
  }
}
