import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, retry, share, switchMap, tap, takeUntil } from 'rxjs/operators'
import { jackportsInfo } from '../models/globalData';
import { Observable, Subject, timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService implements OnDestroy{

 
  games : any[] = [];
  private stopPolling = new Subject();
  private allJackPorts : Observable<any>;


  private url = `http://stage.whgstage.com/front-end-test/games.php`;
  private Jackpotsurl = `http://stage.whgstage.com/front-end-test/jackpots.php`;

 

  constructor(private http: HttpClient) {
    this.allJackPorts = timer(1, 1000).pipe(
      switchMap(() => http.get<jackportsInfo[]>(this.Jackpotsurl)),
      retry(),
      share(),
      takeUntil(this.stopPolling)
    );

  }

  getAllGame(): Observable<jackportsInfo[]> {
    return this.allJackPorts.pipe();
  }

   fetchGames(){
    return this.http.get<any>(this.url)
    .pipe(
      map(result=>{  
      return result;
    }))
  }

  ngOnDestroy() {
    this.stopPolling.next();
}

   
  }

  


